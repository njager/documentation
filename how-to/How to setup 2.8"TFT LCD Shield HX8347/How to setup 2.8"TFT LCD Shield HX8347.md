# How to setup 2.8"TFT LCD Shield HX8347

**T**his *how-to* is attended to help to setup the **2.8"TFT LCD Shield HX8347** with the **Arduino Mega 2650 R3**. If you have the **Arduino Uno**, or if you have **another shield** you can try to follow this how-to.

- **Duration:** short


- **number of steps:** few

### Requirements:

- Arduino Mega 2650 R3,
- 2.8"TFT LCD Shield HX8347,
- Arduino IDE,
- MCUFRIEND_kbv library installed,
- Adafruit GX library installed,
- Adafruit_TouchScreen.

### Pictures of the shield and stylus:

![1549123252](assets/1549123252.png)

![1549123434](assets/1549123434.png)

![1549123476](assets/1549123476.png)



### Assembly of parts:

**I**t's very easy, you just need to plug the shield as on the picture:

![1549123981](assets/1549123981.png)



### Identify the screen:

**T**he **2.8"TFT LCD Shield HX8347** will be identified using the sketch **diagnose_TFT_support** from the **MCUFRIEND_kbv** library.

Upload the sketch and start the **Serial Monitor**, you should see:

![1549131916](assets/1549131916.png)

The valuable information here is the ID of the shield: **0X9595**. If you see **This ID is not supported** you have to enable it.

note: instead of using the console from the IDE, one can use ``picocom``:
```
$ sudo picocom -b 9600 /dev/ttyACM0
```
do not forget to exit ``picocom`` befor uploading a sketch (FYI: ``CTRL+a`` then ``CTRL-x``)



### Enable the shield:

**F**ind and open the file **MCUFRIEND_kbv.cpp**. Then uncomment the line containing:

```
#define SUPPORT_8347D
```

save and close it.



### Test the shield:

**R**e-upload the sketch **diagnose_TFT_support** and start the **Serial Monitor**, you should see:

![1549125866](assets/1549125866.png)

You should also see your LCD screen cycling from blue to green, then red, then gray, then restarting the cycle.



### Set up the touchscreen:

**T**he touchscreen will require some information before you can use it. The sketch **TouchScreen_Calibr_native** will be used to get those information. Please note this sketch comes with the **MCUFRIEND_kbv** library , but it requires you to have the **Adafruit_TouchScreen** library installed. Before you start to upload the sketch, please start the **Serial Monitor**. Then upload the sketch and keep one eye on the **Serial Monitor** and one eye on the **2.8"TFT LCD Shield HX8347**.

On the **Serial Monitor** you should see the ID of the shield. Look at the **2.8"TFT LCD Shield HX8347** you will have instruction to follow. When you've done, you will see the result in **Serial Monitor**(I put red on the interesting part):

![1549126929](assets/1549126929.png)

. You will have to copy these lines in the next part.



### Test the touchscreen:

**S**till from  the **MCUFRIEND_kbv** library, open the sketch **button_simple**. Then you have to replace two lines by those from the **Serial Monitor**. Something like:

![1549127255](assets/1549127255.png)

upload it. When done, you should see:

![1549127771](assets/1549127771.png)



### Contact:

If you want to contact me, please refer to the **README.md**.